<?php

class user_Model
    {
    private $db;
    private $table = 'user';
    public function __construct()
        {
        $this->db = new Database;
        }
    public function get_all()
        {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
        }
    public function get_byId($id)
        {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id =' . $id);
        return $this->db->single();
        }
    public function delete_byId($id)
        {
        $this->db->query('DELETE FROM ' . $this->table . ' WHERE id =' . $id);
        return $this->db->single();
        }
    public function create_Item($data)
        {
        $password = password_hash($data['password'], PASSWORD_DEFAULT);
        $query = "INSERT INTO $this->table (username,password, role, img) VALUES (:username,:password, :role, :img)";
        $this->db->query($query);
        $this->db->bind(':img', $data['img']);
        $this->db->bind(':username', $data['username']);
        $this->db->bind(':role', $data['role']);
        $this->db->bind(':password', $password);
        $this->db->execute();
        return $this->db->rowCount();
        }
    public function update_Item($id, $data)
        {
        $password = password_hash($data['password'], PASSWORD_DEFAULT);
        $query = "UPDATE $this->table SET username=:username, password=:password WHERE id=:id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $password);
        $this->db->execute();
        return $this->db->rowCount();
        }
    public function checkLogin($data)
        {
        $query = "SELECT * FROM " . $this->table . " WHERE username = :username";
        $this->db->query($query);
        $this->db->bind(':username', $data['username']);
        $user = $this->db->single();
        $inputPass = $data['password'];
        $passUserDb = $user['password'];
        if (password_verify($inputPass, $passUserDb)) {
            return $user;
            }
        else {
            return null;
            }
        }

    public function getUserByName($data)
        {
        $query = "SELECT * FROM " . $this->table . " WHERE username=:username";
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->execute();
        return $this->db->single();
        }
    }