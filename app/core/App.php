<?php
// error_reporting(0);
class App
    {

    //Property
    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
        {

        $url = $this->parseURL($_SERVER);

        //Setup Controller
        if (! empty($url) && file_exists('../app/controllers/' . $url[0] . '.php')) {
            $this->controller = $url[0];
            unset($url[0]);
            }
        require_once '../app/controllers/' . $this->controller . '.php';
        $this->controller = new $this->controller;

        //method
        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
                }
            }

        //params
        $this->params = $url ? array_values($url) : [];
        call_user_func_array(
            [$this->controller, $this->method],
            $this->params
        );
        }

    public function parseURL($url)
        {

        $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $path_info = trim($path_info, '/');
        $path_info = str_replace('/index.php', '', $path_info);
        $url = explode('/', $path_info);

        // $uri = trim($_SERVER['REQUEST_URI'], '/');
        // $script = trim($_SERVER['SCRIPT_NAME'], '/');
        // $uri = str_replace('/index.php', '', $uri);
        // $script = str_replace('/index.php', '', $script);
        // $uri = str_replace($script, '', $uri);
        // $url = trim($uri, '/');
        // $url = rtrim($url, '/');
        // $url = filter_var($url, FILTER_SANITIZE_URL);
        // $url = explode('/', $url);

        return $url;

        }
    }