<?php
class Auth extends Controller
    {
    public function Login()
        {
        $data['judul'] = 'login';
        $this->view('templates/header', $data);
        $this->view('auth/login', $data);
        $this->view('templates/footer');
        }

    public function LogProses()
        {

        $login = $this->model('user_Model');

        if (isset($_POST['username']) && isset($_POST['password'])) {
            $data = [
                'username' => $_POST['username'],
                'password' => $_POST['password']
            ];

            $row = $login->checkLogin($data);

            if ($row) {
                $_SESSION['username'] = $row['username'];
                $_SESSION['img'] = $row['img'];
                $_SESSION['role'] = $row['role'];
                $_SESSION['session_login'] = 'login';
                header('location: ' . BASEURL . '/User/Dashboard/');
                }
            else {
                Flasher::setFlash('username/password', 'Salah.', 'danger');
                header('location: ' . BASEURL . '/Auth/Login');
                exit;
                }

            }
        }
    public function Register()
        {
        $data['judul'] = 'Register';
        $this->view('templates/header', $data);
        $this->view('auth/register');
        $this->view('templates/footer');
        }

    public function RegProses()
        {
        $username = $this->model('user_Model')->getUserByName($_POST);

        if (! $username) {
            if ($this->model('user_Model')->create_Item($_POST) > 0) {
                Flasher::setFlash('Berhasil membuat akun', 'Benar.', 'success');
                header('Location:' . BASEURL . '/Auth/Login');
                exit;
                }
            else {
                Flasher::setFlash('Lengkapi Data', 'Salah.', 'danger');
                header('Location:' . BASEURL . '/Auth/Register');
                exit;
                }
            }
        else {
            Flasher::setFlash('Username sama', 'Salah.', 'danger');
            header("Location:" . BASEURL . "/Auth/register");
            exit;
            }
        }

    public function logout()
        {
        session_unset();
        session_destroy();
        header('Location:' . BASEURL . '/auth/Login');
        }
    }