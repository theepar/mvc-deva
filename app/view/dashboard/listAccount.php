<?php
$helper = new Helper();
?>
<div class="container mt-4">
    <h1 class="text-center mb-4">List User</h1>
    <a href="<?= BASEURL; ?>/user/dashboard" class="btn btn-primary mt-3">Kembali</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col" class="text-start">id</th>
                <th scope="col" class="text-center">username</th>
                <th scope="col" class="text-end">role</th>
                <th scope="col" class="text-end">action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['tot'] as $user): ?>
            <tr>
                <th scope="row" class="text-start"><?= $user['id'] ?></th>
                <td class="text-center"><?= $user['username'] ?></td>
                <td class="text-end"><?= $user['role'] ?></td>
                <td class="text-end">
                    <a type="button" class="btn btn-primary updateUser" data-bs-toggle="modal"
                        data-bs-target="#formModal">
                        Update
                    </a>
                    <a href="<?= $url = $helper->url('user/delete/' . $user['id']); ?>"
                        class="btn btn-danger">delete</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Update Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= $helper->url('user/update/' . $data['tot']['id']) ?>" method="POST"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="inputTitle">Title</label>
                        <input type="text" id="inputTitle" class="form-control" name="judul" required
                            value="<?= $data['tot']['judul'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputImage">Image</label>
                        <input type="file" id="inputImage" class="form-control mt-3" name="img" required
                            value="<?= $data['tot']['img'] ?>">
                        <img src="<?= $helper->img($data['tot']['img']) ?>" alt="" class="img-fluid mt-3">
                    </div>
                    <div class="mb-3">
                        <label for="inputCategory">Category</label>
                        <input type="text" id="inputCategory" class="form-control" name="kategori" required
                            value="<?= $data['tot']['kategori'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputDescription">Description</label>
                        <input type="text" id="inputDescription" class="form-control" name="desk" required
                            value="<?= $data['tot']['desk'] ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>