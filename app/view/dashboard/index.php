<?php
$username = $_SESSION['username'];
$img = $_SESSION['img'];
$role = $_SESSION['role'];
Flasher::flash();
$helper = new helper();
?>

<div class="container mt-4">
    <h1 class="text-center mb-4">User Detail</h1>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">
                    <form>
                        <div class="img text-center">
                            <img src="<?= BASEURL; ?>/img/a.png" class="img-fluid" alt=""
                                style="height:300px; border-radius:5px">
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" value="<?= $username ?>" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Moto</label>
                            <p type="email" class="form-control" id="email" readonly> Lorem, ipsum dolor sit amet
                                consectetur adipisicing elit. Quia fuga neque ipsa ipsam rerum
                                doloribus architecto facere! Suscipit, assumenda obcaecati? </p>
                        </div>
                        <div class="mb-3">
                            <label for="role" class="form-label">Role</label>
                            <input type="text" class="form-control" id="role" value="<?= $role ?>" readonly>
                        </div>
                    </form>
                    <div class="btn-back text-center">
                        <a href="<?= BASEURL; ?>/User/Detail" class="btn btn-primary mt-3">List User</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Item</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= BASEURL ?>/About/update/<?= $data['tot']['id'] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="inputTitle">Title</label>
                        <input type="text" id="inputTitle" class="form-control" name="judul" required
                            value="<?= $data['tot']['judul'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputImage">Image</label>
                        <input type="file" id="inputImage" class="form-control mt-3" name="img" required
                            value="<?= $data['tot']['img'] ?>">
                        <img src="<?= BASEURL ?>/uploads/<?= $data['tot']['img'] ?>" alt="" class="img-fluid mt-3">
                    </div>
                    <div class="mb-3">
                        <label for="inputCategory">Category</label>
                        <input type="text" id="inputCategory" class="form-control" name="kategori" required
                            value="<?= $data['tot']['kategori'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputDescription">Description</label>
                        <input type="text" id="inputDescription" class="form-control" name="desk" required
                            value="<?= $data['tot']['desk'] ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>