<?php
Flasher::flash();
$helper = new Helper();
?>
<div class="container mt-5 p-5 bg-light">
    <div class="text-center">
        <img src="<?= $helper->img($data['tot']['img']) ?>" class="img-fluid" alt="" style="height:300px;">
        <h1 class="mt-3 display-4"><?= $data['tot']['judul']; ?></h1>
        <h6 class="card-subtitle mb-2 text-muted"><?= $data['tot']['desk']; ?></h6>
        <a href="<?= BASEURL; ?>/About" class="btn btn-success mt-3">Kembali</a>
        <a href="<?= BASEURL; ?>/About/delete/<?= $data['tot']['id'] ?>" class="btn btn-danger mt-3">Hapus</a>
        <button type="button" class="btn btn-primary mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Update
        </button>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update <?= $data['tot']['judul'] ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= $helper->url('About/update/' . $data['tot']['id']) ?>" method="POST"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="inputTitle">Title</label>
                        <input type="text" id="inputTitle" class="form-control" name="judul" required
                            value="<?= $data['tot']['judul'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputImage">Image</label>
                        <input type="file" id="inputImage" class="form-control mt-3" name="img" required
                            value="<?= $data['tot']['img'] ?>">
                        <img src="<?= $helper->img($data['tot']['img']) ?>" alt="" class="img-fluid mt-3">
                    </div>
                    <div class="mb-3">
                        <label for="inputCategory">Category</label>
                        <input type="text" id="inputCategory" class="form-control" name="kategori" required
                            value="<?= $data['tot']['kategori'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputDescription">Description</label>
                        <input type="text" id="inputDescription" class="form-control" name="desk" required
                            value="<?= $data['tot']['desk'] ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>