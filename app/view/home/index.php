<style>
@supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
    .jumbotron {
        -webkit-backdrop-filter: blur(3px);
        backdrop-filter: blur(3px);
        background-color: rgba(0, 0, 0, 0.5);
    }

    .warning {
        display: none;
    }
}

.jumbotron {
    position: absolute;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 70%;
    border-radius: 15px;
}

.jumbotron h1,
.jumbotron p,
.jumbotron a {
    color: white;
}
</style>


<div class="jumbotron border mt-5 p-5 bg-glass container">
    <h1 class="display-6 text-light fw-medium mb-4">Halo, Selamat Datang!</h1>
    <p class="lead text-light">Perkenalkan, saya user</p>
    <hr class="my-4 text-light border-2">
    <p class="text-light">It uses utility classes for typography and spacing to space content out within the larger
        container.</p>
    <a class="btn btn-outline-light" href="<?= BASEURL . "/about" ?>">Learn More</a>
</div>